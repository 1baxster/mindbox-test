using System;
using System.Collections.Generic;
using System.Linq;
using Mindbox.Geometric.Lib.Interfaces;
using Mindbox.Geometric.Lib.Util;
using Mindbox.Geometric.Lib.Util.Exceptions;

namespace Mindbox.Geometric.Lib.Figure
{
    public class Triangle : IFigure
    {
        private readonly double _sideA;
        private readonly double _sideB;
        private readonly double _sideC;

        private Triangle(double sideA, double sideB, double sideC)
        {
            _sideA = sideA;
            _sideB = sideB;
            _sideC = sideC;
        }

        public double CalculateArea()
        {
            var p = (_sideA + _sideB + _sideC) / 2;
            var area = Math.Sqrt(p * (p - _sideA) * (p - _sideB) * (p - _sideC));

            return area;
        }

        public static Triangle Create(double sideA, double sideB, double sideC)
        {
            AssertSidesLengthAreValid(sideA, sideB, sideC);

            return new Triangle(sideA, sideB, sideC);
        }

        private static void AssertSidesLengthAreValid(double sideA, double sideB, double sideC)
        {
            AssertSideLengthIsNonNegative(sideA, nameof(sideA));
            AssertSideLengthIsNonNegative(sideB, nameof(sideB));
            AssertSideLengthIsNonNegative(sideC, nameof(sideC));

            var sidesFormTriangle = sideA + sideB > sideC && sideA + sideC > sideB && sideB + sideC > sideA;
            if (!sidesFormTriangle)
            {
                throw new InvalidTriangleSidesException(sideA, sideB, sideC);
            }


            void AssertSideLengthIsNonNegative(double side, string sideName)
            {
                if (side <= 0)
                {
                    throw new SideLengthOutOfRangeException(sideName);
                }
            }
        }

        public bool IsRightAngled()
        {
            var sidesCol = new SortedSet<double> { _sideA, _sideB, _sideC };
            var longestSide = sidesCol.Max;
            sidesCol.Remove(longestSide);

            var longestSideSquared = longestSide.Square();
            var sumOfSquaresOfOtherSides = sidesCol.First().Square() + sidesCol.Last().Square();

            // c^2 == a^2 + b^2
            var isRightAngled = longestSideSquared.AreEqual(sumOfSquaresOfOtherSides);

            return isRightAngled;
        }

    }
}