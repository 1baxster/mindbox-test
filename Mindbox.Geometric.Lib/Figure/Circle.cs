using System;
using Mindbox.Geometric.Lib.Interfaces;
using Mindbox.Geometric.Lib.Util;
using Mindbox.Geometric.Lib.Util.Exceptions;

namespace Mindbox.Geometric.Lib.Figure
{
    public class Circle : IFigure
    {
        private readonly double _r;

        private Circle(double r)
        {
            _r = r;
        }

        public double CalculateArea()
        {
            var area = Math.PI * _r.Square();

            return area;
        }

        public static Circle Create(double r)
        {
            return new Circle(r > 0 ? r : throw new RadiusOutOfRangeException(nameof(r)));
        }
    }
}