using System;

namespace Mindbox.Geometric.Lib.Util
{
    internal static class MathUtil
    {
        public static double Square(this double num)
        {
            return num * num;
        }
        
        public static bool AreEqual(this double a, double b)
        {
            var difference = Math.Abs(a * 1E-5);
            
            return Math.Abs(a - b) <= difference;
        }
    }
}