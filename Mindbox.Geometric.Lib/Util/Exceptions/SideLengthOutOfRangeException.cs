﻿using System;

namespace Mindbox.Geometric.Lib.Util.Exceptions
{
    public class SideLengthOutOfRangeException : ArgumentOutOfRangeException
    {
        public SideLengthOutOfRangeException(string paramName) 
            : base(paramName, "Длина стороны должна быть положительной")
        {
        }
    }
}