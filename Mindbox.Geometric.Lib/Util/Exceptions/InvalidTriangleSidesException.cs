﻿using System;

namespace Mindbox.Geometric.Lib.Util.Exceptions
{
    public class InvalidTriangleSidesException : Exception
    {
        public InvalidTriangleSidesException(double sideA, double sideB, double sideC) 
            : base($"Треугольник невозможен с заданными сторонами: [{sideA}, {sideB}, {sideC}]")
        {
        }
    }
}