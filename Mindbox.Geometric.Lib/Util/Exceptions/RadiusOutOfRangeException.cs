﻿using System;

namespace Mindbox.Geometric.Lib.Util.Exceptions
{
    public class RadiusOutOfRangeException : ArgumentOutOfRangeException
    {
        public RadiusOutOfRangeException(string paramName) 
            : base(paramName, "Радиус должен быть положительным")
        {
        }
    }
}