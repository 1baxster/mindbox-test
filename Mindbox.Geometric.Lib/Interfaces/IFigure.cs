﻿namespace Mindbox.Geometric.Lib.Interfaces
{
    public interface IFigure
    {
        double CalculateArea();
    }
}