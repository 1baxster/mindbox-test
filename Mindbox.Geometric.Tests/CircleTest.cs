using System;
using Mindbox.Geometric.Lib.Figure;
using Mindbox.Geometric.Lib.Util.Exceptions;
using Xunit;

namespace Mindbox.Geometric.Tests
{
    public class CircleTest
    {
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ThrowsWhenRadiusIsInvalid(double r)
        {
            Assert.Throws<RadiusOutOfRangeException>(() => Circle.Create(r));
        }

        [Theory]
        [InlineData(1, Math.PI)]
        [InlineData(2, 12.56637061)]
        public void CanCalculateArea(double r, double expectedArea)
        {
            var circle = Circle.Create(r);

            var area = circle.CalculateArea();
            
            Assert.Equal(expectedArea, area, 8);
        }
    }
}