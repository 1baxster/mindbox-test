using System.Collections.Generic;
using System.Linq;
using Mindbox.Geometric.Lib.Figure;
using Mindbox.Geometric.Lib.Interfaces;
using Xunit;

namespace Mindbox.Geometric.Tests
{
    public class PolymorphismTest
    {
        [Fact]
        public void CanCalculateAreaForAnyFigure()
        {
            var figures = new List<IFigure>
            {
                Triangle.Create(3, 4, 5),
                Circle.Create(2),
            };

            var areas = figures.Select(f => f.CalculateArea());
        }
    }
}